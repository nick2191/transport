package nick219.access;

//import org.springframework.stereotype.Component;
import java.sql.*;
import java.util.ArrayList;

//@Component
public class JdbcComponent {

    public ArrayList Method() {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        ArrayList list = new ArrayList();

        try{
            DriverManager.registerDriver(new org.postgresql.Driver());
            con = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/postgres",
                    "1234",
                    "1234"
            );
            stmt = con.createStatement();
            rs = stmt.executeQuery(
                    "select * from location"
            );
            while (rs.next()){
                String id = rs.getString(4);
                list.add(id);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            try { if (con != null) con.close(); } catch (Exception e) {};
        }
        return list;
    }
}
