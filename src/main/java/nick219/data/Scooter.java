package nick219.data;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Scooter extends Transport {

    @Column
    public int power;

    @Column
    public int maxSpeed;

    public Scooter() {
        this.setType(TransportType.SCOOTER);
        this.setCondition(Condition.EXCELLENT);
    }
}
