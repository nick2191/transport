package nick219.data;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class Bike extends Transport {

    public Bike() {
        this.setType(TransportType.BIKE);
        this.setCondition(Condition.EXCELLENT);
    }
}
