package nick219.data;

import lombok.Getter;
import lombok.Setter;
import nick219.data.Parking;

import javax.persistence.*;

@Inheritance(strategy = InheritanceType.JOINED)
@Entity
@Getter
@Setter
@Table(name="transport")
public class Transport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @OneToOne
    private Parking parking;

    @Column
    private int number;

    @Column
    private TransportType type;

    @Column
    private Condition condition;

    // TODO
    //@Column
    //private boolean isFree;

    public enum TransportType {
        BIKE,
        SCOOTER,
        ALL
    }

    public enum Condition {
        EXCELLENT,
        GOOD,
        POOR
    }
}

/* Вариант селекта на такой таблице:
 select t.id, s.max_speed, b.id
    from
        transport t
    left join scooter s on t.id = s.id
    left join bike b on t.id = b.id
where t.id = 1;
 */
