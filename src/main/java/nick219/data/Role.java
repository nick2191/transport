package nick219.data;

import lombok.Data;
//import org.springframework.security.core.GrantedAuthority;
import javax.persistence.*;
//import javax.validation.constraints.Size;

@Entity
@Table(name="role")
@Data
public class Role /*implements GrantedAuthority*/ {

    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    //@Size(min = 2, message = "Не менее двух символов")
    private String name;

    /*
    @Override
    public String getAuthority() {
        return name;
    }
    */
}