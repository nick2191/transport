package nick219.data;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name="rent")
public class Rent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @OneToOne
    private Parking startLocation;

    @OneToOne
    private Parking finalLocation;

    @ManyToMany(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Set<User> user;

    @OneToOne
    private Transport transport;

    @Column
    private Date startDate;
    // Дмитрий рекомендовал тип (вместо Date)
    // private LocalDateTime startDate1;

    @Column
    private Date finalDate;

    @Column
    private boolean isOpen;
}
