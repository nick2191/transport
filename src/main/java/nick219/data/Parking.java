package nick219.data;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name="parking")
public class
Parking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @OneToOne
    private Location location;

    @Column
    private int number;

    @Column
    private int radius;

    @Column
    private Transport.TransportType permittedTransport;

    @Column
    private String name;
}