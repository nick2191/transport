package nick219.controllers;

import nick219.data.UserAccount;
import nick219.service.AccountService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class SecurityFilter implements Filter {
    FilterConfig config;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.config = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpSession session = req.getSession(false);
        String servletPath = ((HttpServletRequest) servletRequest).getServletPath();
        UserAccount loginedUser = AccountService.getLoginedUser(((HttpServletRequest) servletRequest).getSession());

        if (servletPath.equals("/hi") || servletPath.equals("/login")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        if (session != null) {
            if (loginedUser != null || loginedUser.getData() == "1") {
                filterChain.doFilter(
                        servletRequest, servletResponse);
            }
        } else {
            throw new ServletException("No Auth");
        }
    }

    @Override
    public void destroy() {
    }
}